use crate::{Dims, Error};

/// Helper type used to define the source of `ImageSource`'s data
#[derive(Clone)]
pub struct ImageSource<'a> {
    _unused: &'a (),
    /// An already loaded image that is passed directly to the generator
    image: image::RgbaImage,
}

impl<'a> From<image::RgbaImage> for ImageSource<'a> {
    fn from(img: image::RgbaImage) -> Self {
        ImageSource {
            _unused: &(),
            image: img,
        }
    }
}

/// Helper type used to mask `ImageSource`'s channels
#[derive(Clone, Copy)]
pub enum ChannelMask {
    R,
    G,
    B,
    A,
}

pub(crate) fn load_image(
    src: ImageSource<'_>,
    resize: Option<Dims>,
) -> Result<image::RgbaImage, Error> {
    let img = src.image;
    if let Some(size) = &resize {
        if img.width() != size.width || img.height() != size.height {
            panic!();
        }
    }
    Ok(img)
}
