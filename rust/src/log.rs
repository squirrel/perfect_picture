// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

#[link(name = "log")]
extern "C" {
    fn __android_log_write(
        prio: std::os::raw::c_int,
        tag: *const std::os::raw::c_char,
        text: *const std::os::raw::c_char,
    ) -> std::os::raw::c_int;
}

pub trait Message: Into<Vec<u8>> {}

#[allow(unused)]
#[deprecated(note = "Remember to remove your debug logging when you're done.")]
pub fn log(message: impl Message) {
    let message = std::ffi::CString::new(message).unwrap();
    unsafe {
        __android_log_write(3, "xxx\0".as_ptr(), message.as_ptr());
    }
}

impl Message for &str {}

impl Message for String {}
