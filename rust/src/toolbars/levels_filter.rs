// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::{Button, SIZES_ZERO};
use crate::{action::Action, gesture::Channel, types::Choice};

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    let channel = match context.toolbar_index {
        0 => Some(Channel::Master),
        1 => Some(Channel::Red),
        2 => Some(Channel::Green),
        3 => Some(Channel::Blue),
        _ => None,
    };
    if let Some(channel) = channel {
        return Button::LevelsFilter(channel);
    }
    context.toolbar_index -= 4;
    fallthrough!(context.feather_item(
        context.picture.levels_filter_feather1,
        context.picture.levels_filter_feather2,
        |item_index| Action::LevelsFilterFeather(Choice::Setting1, SIZES_ZERO[item_index]),
        |item_index| Action::LevelsFilterFeather(Choice::Setting2, SIZES_ZERO[item_index]),
    ));
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::ClearMask,
            1 => Action::ApplyLevelsFilter,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    super::levels_common::draw_levels(&display.inner, display.picture.levels_filter_levels);
    display.draw_feather(
        display.picture.levels_filter_feather1,
        display.picture.levels_filter_feather2,
    );
    let mut items = display.draw_items(2);
    items.draw_item("Cancel");
    items.draw_apply_item(display.picture);
}
