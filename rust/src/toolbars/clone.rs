// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::{Button, SIZES_NONZERO, SIZES_ZERO};
use crate::action::Action;

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item_cutoff(
            SIZES_ZERO,
            "Inpaint outline",
            context.picture.clone_inpaint_outline as u16 / 2,
            |item_index| Action::CloneInpaintOutline((2 * SIZES_ZERO[item_index]) as _),
        );
    }
    if context.toolbar() {
        return context.item_cutoff(
            SIZES_NONZERO,
            "Inpaint context",
            context.picture.clone_inpaint_context_size as _,
            |item_index| Action::CloneInpaintContextSize(SIZES_NONZERO[item_index] as _),
        );
    }
    if context.toolbar() {
        return context.item(3, |item_index| match item_index {
            0 => Action::CancelClone,
            1 => Action::CloneOffsetSelected,
            2 => Action::ApplyClone,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    display.draw_items_cutoff(
        SIZES_ZERO,
        "Inpaint outline",
        (display.picture.clone_inpaint_outline as u16) / 2,
    );
    display.draw_items_cutoff(
        SIZES_NONZERO,
        "Inpaint context",
        display.picture.clone_inpaint_context_size as _,
    );
    let mut items = display.draw_items(3);
    items.draw_item("Cancel");
    items.draw_item(&format!(
        "Offset {},{}",
        display.picture.clone_offset.x.round() as i32,
        display.picture.clone_offset.y.round() as i32
    ));
    items.draw_apply_item(display.picture);
    if display.picture.clone_offset_selected {
        items.draw_underline(1);
    }
}
