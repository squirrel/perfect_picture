// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    bindings,
    toolbars::SIZES_ZERO,
    types::{Choice, Picture, Point},
};

pub struct Display<'a> {
    pub inner: DisplayInner<'a>,
    pub picture: &'a mut Picture,
}

pub struct DisplayInner<'a> {
    pub align_center: bindings::Align<'a>,
    pub align_left: bindings::Align<'a>,
    pub align_right: bindings::Align<'a>,
    pub canvas: bindings::Canvas<'a>,
    pub density: f32,
    pub env: jni::JNIEnv<'a>,
    pub paint: bindings::Paint<'a>,
    pub position_y_max: std::cell::Cell<i32>,
    pub toolbar_size_y: i32,
    pub view_size_x: i32,
}

pub struct Items<'a, 'b> {
    display: &'a DisplayInner<'b>,
    item_distance: f32,
    item_index: usize,
    position_x_min: f32,
    pub position_y: f32,
}

impl<'a> std::ops::Deref for Display<'a> {
    type Target = DisplayInner<'a>;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl Display<'_> {
    pub fn draw_feather(&mut self, feather1: u16, feather2: u16) {
        if self.picture.intersect_masks {
            self.draw_items_cutoff(SIZES_ZERO, "Feather 1", feather1);
            self.draw_items_cutoff(SIZES_ZERO, "Feather 2", feather2);
        } else {
            self.draw_items_cutoff(SIZES_ZERO, "Feather", feather1);
        }
    }
}

impl<'a> DisplayInner<'a> {
    pub fn draw_label(&self, item_count: usize, text: &str) -> Items<'_, 'a> {
        self.position_y_max
            .set(self.position_y_max.get() + self.toolbar_size_y);
        let position_y = self.position_y_max.get() as f32 - (20. * self.density).round();
        let text = bindings::string(self.env, text);
        self.paint.set_text_align(self.env, self.align_left);
        let margin = (8. * self.density).round();
        self.canvas.draw_text(
            self.env,
            text,
            Point {
                x: margin,
                y: position_y,
            },
            self.paint,
        );
        self.paint.set_text_align(self.env, self.align_center);
        let position_x_min = 2. * margin + self.paint.measure_text(self.env, text);
        Items {
            display: self,
            item_distance: (self.view_size_x as f32 - position_x_min) / item_count as f32,
            item_index: 0,
            position_x_min,
            position_y,
        }
    }

    pub fn draw_items(&self, item_count: usize) -> Items<'_, 'a> {
        self.position_y_max
            .set(self.position_y_max.get() + self.toolbar_size_y);
        let item_distance = self.view_size_x as f32 / item_count as f32;
        Items {
            display: self,
            item_distance,
            item_index: 0,
            position_x_min: 0.,
            position_y: self.position_y_max.get() as f32 - (20. * self.density).round(),
        }
    }

    pub fn draw_items_cutoff_internal(
        &self,
        items: &[u16],
        label: &str,
        selected_item_index: Option<usize>,
    ) {
        let item_index_min = match selected_item_index {
            None => 0,
            Some(item_index) => item_index
                .saturating_sub(4)
                .min(items.len().saturating_sub(9)),
        };
        let items = &items[item_index_min..items.len().min(item_index_min + 9)];
        let mut items_display = self.draw_label(items.len(), label);
        for value in items {
            items_display.draw_item(&value.to_string());
        }
        if let Some(item_index) = selected_item_index {
            items_display.draw_underline(item_index - item_index_min);
        }
    }

    pub fn draw_items_cutoff(&self, items: &[u16], label: &str, selected_value: u16) {
        self.draw_items_cutoff_internal(
            items,
            label,
            items.iter().position(|&value| value == selected_value),
        );
    }

    pub fn draw_opacity_items(&self, label: &str, selected_value: u8) {
        self.draw_items_cutoff_internal(
            super::MASK_OPACITIES,
            label,
            super::MASK_OPACITIES
                .iter()
                .position(|&value| 255 * value / 100 == selected_value as u16),
        );
    }

    pub fn draw_setting(&self, choice: Choice) {
        let mut items = self.draw_items(2);
        items.draw_item("Setting 1");
        items.draw_item("Setting 2");
        items.draw_underline(match choice {
            Choice::Setting1 => 0,
            Choice::Setting2 => 1,
        });
    }
}

impl Items<'_, '_> {
    pub fn draw_apply_item(&mut self, picture: &Picture) {
        self.draw_item(if picture.working {
            "Working"
        } else if picture.mask.value.is_empty() && picture.undo.is_some() {
            "Undo"
        } else {
            "Apply"
        });
    }

    pub fn draw_item(&mut self, label: &str) {
        self.display.canvas.draw_text(
            self.display.env,
            bindings::string(self.display.env, label),
            Point {
                x: self.item_position_x(self.item_index),
                y: self.position_y,
            },
            self.display.paint,
        );
        self.item_index += 1;
    }

    pub fn draw_underline(&self, item_index: usize) {
        let position_x = self.item_position_x(item_index);
        let size_x = 12. * self.display.density;
        self.display.canvas.draw_rectangle(
            self.display.env,
            position_x - size_x,
            self.position_y + 6. * self.display.density,
            position_x + size_x,
            self.position_y + 8. * self.display.density,
            self.display.paint,
        );
    }

    pub fn item_position_x(&self, item_index: usize) -> f32 {
        self.position_x_min + self.item_distance * (item_index as f32 + 0.5)
    }
}
