// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Button;
use crate::action::Action;

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item(3, |item_index| match item_index {
            0 => Action::Open,
            1 => Action::ExportToolbar,
            2 => Action::TransformToolbar,
            _ => panic!(),
        });
    }
    if context.toolbar() {
        return context.item(3, |item_index| match item_index {
            0 => Action::CropToolbar,
            1 => Action::LevelsToolbar,
            2 => Action::SplitScreen,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    let draw_items = |items: &[&str]| {
        let mut items_display = display.draw_items(items.len());
        for item in items.iter() {
            items_display.draw_item(item);
        }
        items_display
    };
    draw_items(&["Open", "Export", "Transform"]);
    let items_display = draw_items(&["Crop", "Levels", "Split screen"]);
    if display.picture.split_screen {
        items_display.draw_underline(2);
    }
}
