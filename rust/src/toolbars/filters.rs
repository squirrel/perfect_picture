// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Button;
use crate::action::Action;

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::LightToolbar,
            1 => Action::LevelsFilterToolbar,
            _ => panic!(),
        });
    }
    if context.toolbar() {
        return context.item(3, |item_index| match item_index {
            0 => Action::BlurToolbar,
            1 => Action::UnsharpMaskToolbar,
            2 => Action::BandStopToolbar,
            _ => panic!(),
        });
    }
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::CloneToolbar,
            1 => Action::InpaintToolbar,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    let items = ["Light", "Levels"];
    let mut items_display = display.draw_items(items.len());
    for item in items.iter() {
        items_display.draw_item(item);
    }
    let items = ["Blur", "Unsharp mask", "Band-stop"];
    let mut items_display = display.draw_items(items.len());
    for item in items.iter() {
        items_display.draw_item(item);
    }
    let items = ["Clone", "Inpaint"];
    let mut items_display = display.draw_items(items.len());
    for item in items.iter() {
        items_display.draw_item(item);
    }
}
