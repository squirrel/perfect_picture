// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Button;
use crate::{action::Action, bindings, gesture::Channel, types::Choice};

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    let channel = match context.toolbar_index {
        3 => Some(Channel::Master),
        4 => Some(Channel::Red),
        5 => Some(Channel::Green),
        6 => Some(Channel::Blue),
        _ => None,
    };
    if let Some(channel) = channel {
        return Button::Levels(channel);
    }
    context.toolbar_index -= 7;
    if context.toolbar_index < 0 {
        return Button::None;
    }
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::LevelsSetting(Choice::Setting1),
            1 => Action::LevelsSetting(Choice::Setting2),
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    let picture = &mut display.picture;
    let display = &display.inner;
    let margin = (8. * display.density).round();
    let histogram = &picture.histogram;
    let histogram_size_y = 3 * display.toolbar_size_y;
    display
        .position_y_max
        .set(display.position_y_max.get() + histogram_size_y);
    let env = display.env;
    let image = picture
        .histogram_image
        .get_or_insert_with(|| histogram_image(env, histogram, histogram_size_y));
    let source = bindings::Rect::new(display.env, 0, 0, 256, histogram_size_y);
    let destination = bindings::RectF::new(
        display.env,
        margin,
        (display.position_y_max.get() - histogram_size_y) as _,
        display.view_size_x as f32 - margin,
        display.position_y_max.get() as _,
    );
    display.canvas.draw_bitmap(
        display.env,
        image.as_obj(),
        source,
        destination,
        bindings::null(),
    );
    super::levels_common::draw_levels(display, picture.levels.value);
    display.draw_setting(picture.levels.choice);
}

fn histogram_image(
    env: jni::JNIEnv,
    histogram: &[[u32; 3]; 256],
    size_y: i32,
) -> bindings::GlobalBitmap {
    let max_value = histogram
        .iter()
        .map(|values| values.iter().copied().sum::<u32>())
        .max()
        .unwrap_or(1);
    let scale = size_y as f32 / max_value as f32;
    let mut image_data = vec![0; 256 * size_y as usize];
    for (level, values) in histogram.iter().enumerate() {
        let value = values.iter().copied().sum::<u32>() as f32;
        let position_y_min = size_y - (scale * value).round() as i32;
        let red_position_x_min = size_y - (scale * values[0] as f32).round() as i32;
        let green_position_x_min = size_y - (scale * values[1] as f32).round() as i32;
        let blue_position_x_min = size_y - (scale * values[2] as f32).round() as i32;
        for position_y in position_y_min..size_y {
            let red = if position_y < red_position_x_min {
                0
            } else {
                0xff0000
            };
            let green = if position_y < green_position_x_min {
                0
            } else {
                0xff00
            };
            let blue = if position_y < blue_position_x_min {
                0
            } else {
                0xff
            };
            let color = red | green | blue;
            image_data[256 * position_y as usize + level] = if color == 0 {
                0xff7f7f7fu32 as i32
            } else {
                -0x1000000 | color
            };
        }
    }
    let image_data_array = env.new_int_array(256 * size_y).unwrap();
    env.set_int_array_region(image_data_array, 0, &image_data)
        .unwrap();
    bindings::Bitmap::create_bitmap_initialized(
        env,
        image_data_array.into(),
        crate::types::Size { x: 256, y: size_y },
        bindings::BitmapConfig::argb_8888(env),
    )
    .global(env)
}
