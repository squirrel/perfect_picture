// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Button;
use crate::{action::Action, types::Format};

const EXPORT_FORMATS: &[&str] = &["JPEG", "PNG", "WebP"];
const EXPORT_QUALITIES: &[u16] = super::MASK_OPACITIES;

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return if context.picture.scaled_down_size().is_some() {
            context.item_label(2, "Resolution", |item_index| {
                Action::ExportScaleDown(item_index > 0)
            })
        } else {
            Button::None
        };
    }
    if context.toolbar() {
        return context.item_label(EXPORT_FORMATS.len(), "Format", |item_index| {
            Action::ExportFormat(match item_index {
                0 => Format::Jpeg,
                1 => Format::Png,
                2 => Format::Webp,
                _ => panic!(),
            })
        });
    }
    if context.toolbar() {
        return context.item_cutoff(
            EXPORT_QUALITIES,
            "Quality",
            context.picture.export_quality as _,
            |item_index| Action::ExportQuality(EXPORT_QUALITIES[item_index] as _),
        );
    }
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::Export,
            1 => Action::Share,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    let scaled_down_size = display.picture.scaled_down_size();
    let item_count = match scaled_down_size {
        None => 1,
        Some(_) => 2,
    };
    let mut items = display.draw_label(item_count, "Resolution");
    items.draw_item(&display.picture.crop.value.size().format());
    if let Some(size) = scaled_down_size {
        items.draw_item(&size.format());
    }
    items.draw_underline(display.picture.export_scale_down as _);
    let mut items = display.draw_label(EXPORT_FORMATS.len(), "Format");
    for value in EXPORT_FORMATS {
        items.draw_item(value);
    }
    items.draw_underline(match display.picture.export_format {
        Format::Jpeg => 0,
        Format::Png => 1,
        Format::Webp => 2,
    });
    display.draw_items_cutoff(
        EXPORT_QUALITIES,
        "Quality",
        display.picture.export_quality as _,
    );
    let mut items = display.draw_items(2);
    items.draw_item("Save");
    items.draw_item("Share");
}
