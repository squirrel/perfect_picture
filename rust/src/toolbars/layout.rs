// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    bindings,
    toolbars::SIZES_ZERO,
    types::{BoundingBox, ButtonGesture},
};

pub enum Button {
    Fallthrough,
    Levels(crate::gesture::Channel),
    LevelsFilter(crate::gesture::Channel),
    None,
    Some(ButtonGesture),
}

pub struct Context<'a> {
    pub density: f32,
    pub env: jni::JNIEnv<'a>,
    pub picture: &'a crate::types::Picture,
    pub position_x: f32,
    pub position_y_min: f32,
    pub position_y_max: f32,
    pub toolbar_index: i32,
    pub view: bindings::View<'a>,
}

type Callback = fn(usize) -> crate::action::Action;

impl Context<'_> {
    pub fn feather_item(
        &mut self,
        feather1: u16,
        feather2: u16,
        setting1_callback: Callback,
        setting2_callback: Callback,
    ) -> Button {
        if self.picture.intersect_masks {
            if self.toolbar() {
                return self.item_cutoff(SIZES_ZERO, "Feather 1", feather1, setting1_callback);
            }
            if self.toolbar() {
                return self.item_cutoff(SIZES_ZERO, "Feather 1", feather2, setting2_callback);
            }
        } else {
            if self.toolbar() {
                return self.item_cutoff(SIZES_ZERO, "Feather", feather1, setting1_callback);
            }
        }
        Button::Fallthrough
    }

    pub fn item(&self, item_count: usize, callback: Callback) -> Button {
        let view_size_x = self.view.get_width(self.env);
        let item_distance = view_size_x as f32 / item_count as f32;
        let item_index = (self.position_x / item_distance) as usize;
        Button::Some(ButtonGesture {
            action: callback(item_index),
            bounding_box: BoundingBox {
                position_x_min: item_index as f32 * item_distance,
                position_x_max: (item_index + 1) as f32 * item_distance,
                position_y_min: self.position_y_min,
                position_y_max: self.position_y_max,
            },
        })
    }

    pub fn item_cutoff(
        &self,
        items: &[u16],
        label: &str,
        selected_value: u16,
        callback: Callback,
    ) -> Button {
        self.item_cutoff_internal(
            items,
            label,
            items.iter().position(|&value| value == selected_value),
            callback,
        )
    }

    pub fn item_cutoff_internal(
        &self,
        items: &[u16],
        label: &str,
        selected_item_index: Option<usize>,
        callback: Callback,
    ) -> Button {
        let item_index_min = match selected_item_index {
            None => 0,
            Some(item_index) => item_index
                .saturating_sub(4)
                .min(items.len().saturating_sub(9)),
        };
        self.item_label_internal(items.len().min(9), item_index_min, label, callback)
    }

    pub fn item_label(&self, item_count: usize, label: &str, callback: Callback) -> Button {
        self.item_label_internal(item_count, 0, label, callback)
    }

    pub fn item_label_internal(
        &self,
        item_count: usize,
        item_index_min: usize,
        label: &str,
        callback: Callback,
    ) -> Button {
        let paint = bindings::Paint::new(self.env);
        paint.set_text_size(self.env, super::TEXT_SIZE * self.density);
        let item_position_x_min =
            16. * self.density + paint.measure_text(self.env, bindings::string(self.env, label));
        if self.position_x >= item_position_x_min {
            let view_size_x = self.view.get_width(self.env);
            let item_distance = (view_size_x as f32 - item_position_x_min) / item_count as f32;
            let item_index = ((self.position_x - item_position_x_min) / item_distance) as usize;
            Button::Some(ButtonGesture {
                action: callback(item_index_min + item_index),
                bounding_box: BoundingBox {
                    position_x_min: item_index as f32 * item_distance,
                    position_x_max: (item_index + 1) as f32 * item_distance,
                    position_y_min: self.position_y_min,
                    position_y_max: self.position_y_max,
                },
            })
        } else {
            Button::None
        }
    }

    pub fn opacity_item(&self, label: &str, selected_value: u8, callback: Callback) -> Button {
        self.item_cutoff_internal(
            super::MASK_OPACITIES,
            label,
            super::MASK_OPACITIES
                .iter()
                .position(|&value| (255 * value / 100) as u8 == selected_value),
            callback,
        )
    }

    pub fn toolbar(&mut self) -> bool {
        if self.toolbar_index == 0 {
            true
        } else {
            self.toolbar_index -= 1;
            false
        }
    }
}
