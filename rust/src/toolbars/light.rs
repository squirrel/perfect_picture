// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::{Button, SIZES_ZERO};
use crate::{action::Action, types::Choice};

const OPTIONS: &[u16] = &[
    0, 1, 2, 3, 4, 5, 6, 8, 10, 12, 16, 20, 24, 32, 40, 48, 64, 80, 100,
];

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item_cutoff(
            OPTIONS,
            "Lighten",
            context.picture.light.brightness as _,
            |item_index| Action::LightBrightness(OPTIONS[item_index] as _),
        );
    }
    if context.toolbar() {
        return context.item_cutoff(
            OPTIONS,
            "Darken",
            (-context.picture.light.brightness) as _,
            |item_index| Action::LightBrightness(-(OPTIONS[item_index] as i8)),
        );
    }
    if context.toolbar() {
        return context.item_cutoff(
            OPTIONS,
            "Saturate",
            context.picture.light.saturation as _,
            |item_index| Action::LightSaturation(OPTIONS[item_index] as _),
        );
    }
    if context.toolbar() {
        return context.item_cutoff(
            OPTIONS,
            "Desaturate",
            (-context.picture.light.saturation) as _,
            |item_index| Action::LightSaturation(-(OPTIONS[item_index] as i8)),
        );
    }
    fallthrough!(context.feather_item(
        context.picture.light_feather1,
        context.picture.light_feather2,
        |item_index| Action::LightFeather(Choice::Setting1, SIZES_ZERO[item_index]),
        |item_index| Action::LightFeather(Choice::Setting2, SIZES_ZERO[item_index]),
    ));
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::ClearMask,
            1 => Action::ApplyLight,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    display.draw_items_cutoff(OPTIONS, "Lighten", display.picture.light.brightness as _);
    display.draw_items_cutoff(OPTIONS, "Darken", (-display.picture.light.brightness) as _);
    display.draw_items_cutoff(OPTIONS, "Saturate", display.picture.light.saturation as _);
    display.draw_items_cutoff(
        OPTIONS,
        "Desaturate",
        (-display.picture.light.saturation) as _,
    );
    display.draw_feather(
        display.picture.light_feather1,
        display.picture.light_feather2,
    );
    let mut items = display.draw_items(2);
    items.draw_item("Cancel");
    items.draw_apply_item(display.picture);
}
