// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Button;
use crate::{action::Action, types::Choice};

const MASK_COLORS: &[u32] = &[
    0xffff0000, 0xffff00ff, 0xff0000ff, 0xff00ffff, 0xff00ff00, 0xff000000,
];

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item_label(MASK_COLORS.len(), "Mask color", |item_index| {
            Action::MaskColor(MASK_COLORS[item_index])
        });
    }
    if context.toolbar() {
        return context.opacity_item(
            "Mask opacity",
            (context.picture.mask_color >> 24) as _,
            |item_index| {
                Action::MaskOpacity((255 * super::MASK_OPACITIES[item_index] as u32 / 100) as _)
            },
        );
    }
    if context.toolbar() {
        return context.item(4, |item_index| match item_index {
            0 => Action::ClearMask,
            1 => Action::IntersectMasks,
            2 => Action::MaskSetting(Choice::Setting1),
            3 => Action::MaskSetting(Choice::Setting2),
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    let items = display.draw_label(MASK_COLORS.len(), "Mask color");
    let radius = 8. * display.density;
    for (index, &color) in MASK_COLORS.iter().enumerate() {
        display.paint.set_color(display.env, color);
        display.canvas.draw_circle(
            display.env,
            crate::types::Point {
                x: items.item_position_x(index),
                y: items.position_y,
            },
            radius,
            display.paint,
        );
    }
    display.paint.set_color(display.env, super::TEXT_COLOR);
    if let Some(item_index) = MASK_COLORS
        .iter()
        .position(|&color| (color ^ display.picture.mask_color) & 0xffffff == 0)
    {
        items.draw_underline(item_index);
    }
    display.draw_opacity_items("Mask opacity", (display.picture.mask_color >> 24) as _);
    let mut items = display.draw_items(4);
    items.draw_item("Clear");
    items.draw_item("Intersect");
    items.draw_item("Mask 1");
    items.draw_item("Mask 2");
    if display.picture.intersect_masks {
        items.draw_underline(1);
    }
    items.draw_underline(match display.picture.mask.choice {
        Choice::Setting1 => 2,
        Choice::Setting2 => 3,
    });
}
