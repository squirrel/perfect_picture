// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::{Button, SIZES_NONZERO, SIZES_ZERO};
use crate::{action::Action, types::Choice};

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item_cutoff(
            SIZES_NONZERO,
            "Course",
            context.picture.band_stop_course,
            |item_index| Action::BandStopCourse(SIZES_NONZERO[item_index]),
        );
    }
    if context.toolbar() {
        return context.item_cutoff(
            SIZES_NONZERO,
            "Fine",
            context.picture.band_stop_fine,
            |item_index| Action::BandStopFine(SIZES_NONZERO[item_index]),
        );
    }
    if context.toolbar() {
        return context.opacity_item("Opacity", context.picture.band_stop_opacity, |item_index| {
            Action::BandStopOpacity((255 * super::MASK_OPACITIES[item_index] / 100) as _)
        });
    }
    fallthrough!(context.feather_item(
        context.picture.band_stop_feather1,
        context.picture.band_stop_feather2,
        |item_index| Action::BandStopFeather(Choice::Setting1, SIZES_ZERO[item_index]),
        |item_index| Action::BandStopFeather(Choice::Setting2, SIZES_ZERO[item_index]),
    ));
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::ClearMask,
            1 => Action::ApplyBandStop,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    display.draw_items_cutoff(SIZES_NONZERO, "Course", display.picture.band_stop_course);
    display.draw_items_cutoff(SIZES_NONZERO, "Fine", display.picture.band_stop_fine);
    display.draw_opacity_items("Opacity", display.picture.band_stop_opacity);
    display.draw_feather(
        display.picture.band_stop_feather1,
        display.picture.band_stop_feather2,
    );
    let mut items = display.draw_items(2);
    items.draw_item("Cancel");
    items.draw_apply_item(display.picture);
}
