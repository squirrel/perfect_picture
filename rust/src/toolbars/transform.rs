// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::Button;
use crate::action::Action;

const TRANSFORM_OPTIONS: &[&str] = &["90°", "180°", "270°", "left–right", "up–down"];

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item(TRANSFORM_OPTIONS.len(), |item_index| match item_index {
            0 => Action::Rotate90,
            1 => Action::Rotate180,
            2 => Action::Rotate270,
            3 => Action::FlipX,
            4 => Action::FlipY,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    let mut items = display.draw_items(TRANSFORM_OPTIONS.len());
    for value in TRANSFORM_OPTIONS {
        items.draw_item(value);
    }
}
