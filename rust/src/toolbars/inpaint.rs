// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use super::{Button, SIZES_NONZERO};
use crate::action::Action;

pub fn action_at_point(context: &mut super::layout::Context) -> Button {
    if context.toolbar() {
        return context.item_cutoff(
            SIZES_NONZERO,
            "Inpaint context",
            context.picture.inpaint_context_size as _,
            |item_index| Action::InpaintContextSize(SIZES_NONZERO[item_index] as _),
        );
    }
    if context.toolbar() {
        return context.item(2, |item_index| match item_index {
            0 => Action::ClearMask,
            1 => Action::ApplyInpaint,
            _ => panic!(),
        });
    }
    Button::Fallthrough
}

pub fn draw(display: &mut super::display::Display) {
    display.draw_items_cutoff(
        SIZES_NONZERO,
        "Inpaint context",
        display.picture.inpaint_context_size as _,
    );
    let mut items = display.draw_items(2);
    items.draw_item("Cancel");
    items.draw_apply_item(display.picture);
}
