// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{bindings::string, types::Point};

pub fn draw_levels(display: &super::display::DisplayInner, levels: crate::types::Levels) {
    let margin = (8. * display.density).round();
    let draw_levels = |levels: (u8, u8), next_color| {
        display
            .position_y_max
            .set(display.position_y_max.get() + display.toolbar_size_y);
        let transform =
            |level| (display.view_size_x as f32 - 2. * margin) * level as f32 / 255. + margin;
        display
            .paint
            .set_text_align(display.env, display.align_left);
        let text_position_y = display.position_y_max.get() as f32 - 16. * display.density;
        display.canvas.draw_text(
            display.env,
            string(display.env, &levels.0.to_string()),
            Point {
                x: margin,
                y: text_position_y,
            },
            display.paint,
        );
        display
            .paint
            .set_text_align(display.env, display.align_right);
        display.canvas.draw_text(
            display.env,
            string(display.env, &levels.1.to_string()),
            Point {
                x: display.view_size_x as f32 - margin,
                y: text_position_y,
            },
            display.paint,
        );
        display.canvas.draw_rectangle(
            display.env,
            transform(levels.0),
            display.position_y_max.get() as f32 - 12. * display.density,
            transform(levels.1),
            display.position_y_max.get() as f32 - 4. * display.density,
            display.paint,
        );
        display.paint.set_color(display.env, next_color);
    };
    draw_levels(levels.master(), 0xffff0000);
    draw_levels(levels.red, 0xff00ff00);
    draw_levels(levels.green, 0xff0000ff);
    draw_levels(levels.blue, super::TEXT_COLOR);
    display
        .paint
        .set_text_align(display.env, display.align_center);
}
