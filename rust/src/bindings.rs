// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::types::{BoundingBox, Point, Size};
use jni::{
    objects::{GlobalRef, JByteBuffer, JObject},
    JNIEnv,
};

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

impl<'a> From<JByteBuffer<'a>> for Buffer<'a> {
    fn from(source: JByteBuffer<'a>) -> Self {
        Buffer(source.into())
    }
}

impl Canvas<'_> {
    pub fn draw_color(self, env: JNIEnv, color: u32) {
        self.draw_color_internal(env, color as _)
    }
}

impl<'a> From<ColorMatrixColorFilter<'a>> for ColorFilter<'a> {
    fn from(source: ColorMatrixColorFilter<'a>) -> Self {
        ColorFilter(source.0)
    }
}

impl<'a> From<BlurMaskFilter<'a>> for MaskFilter<'a> {
    fn from(source: BlurMaskFilter<'a>) -> Self {
        MaskFilter(source.0)
    }
}

impl GlobalBitmap {
    pub fn clone(&self) -> Self {
        GlobalBitmap(self.0.clone())
    }
}

impl Paint<'_> {
    pub fn set_color(self, env: JNIEnv, color: u32) {
        self.set_color_internal(env, color as _)
    }
}

impl<'a> From<PorterDuffTransferMode<'a>> for TransferMode<'a> {
    fn from(source: PorterDuffTransferMode<'a>) -> Self {
        TransferMode(source.0)
    }
}

impl<'a> View<'a> {
    pub fn get_display_metrics(self, env: JNIEnv<'a>) -> DisplayMetrics<'a> {
        self.get_resources(env).get_display_metrics(env)
    }
}

pub trait Null {
    fn null() -> Self;
}

pub fn null<T: Null>() -> T {
    Null::null()
}

pub fn string<'a>(env: JNIEnv<'a>, string: &str) -> String<'a> {
    String(*env.new_string(string).unwrap())
}
