// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::types::SplitId;

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeScale(
    env: jni::JNIEnv,
    _caller: crate::bindings::Context,
    focus_x: f32,
    focus_y: f32,
    span: f32,
    view: crate::bindings::View,
) {
    let mut state = crate::state();
    let state = state.get();
    if let Some(picture) = &mut state.picture {
        if let Some(crate::gesture::Gesture::Pan(gesture)) = &state.gesture {
            let density = view.get_display_metrics(env).scaled_density(env);
            let buttons_size_y = (48. * density).round() as i32;
            let buttons_position_y_min = view.get_height(env) - state.inset_bottom - buttons_size_y;
            let viewport_size_x = view.get_width(env);
            let viewport_size_y = buttons_position_y_min - state.inset_top;
            let size = picture.size;
            let default_scale =
                if picture.size.x * viewport_size_y > picture.size.y * viewport_size_x {
                    viewport_size_x as f32 / size.x as f32
                } else {
                    viewport_size_y as f32 / size.y as f32
                };
            let scale = (gesture.scale * span).min(64.).max(default_scale / 2.);
            let center_x = viewport_size_x as f32 / 2.;
            let center_y = if picture.split_screen {
                match gesture.split_id {
                    SplitId::Split1 => state.inset_top + picture.split_position_y,
                    SplitId::Split2 => buttons_position_y_min + picture.split_position_y + 1,
                }
            } else {
                state.inset_top + viewport_size_y
            } as f32
                / 2.;
            let size = picture.size;
            let split = picture.split_mut(gesture.split_id);
            split.pan_x = (scale * gesture.pan_x + focus_x)
                .min(center_x)
                .max(center_x - scale * size.x as f32);
            split.pan_y = (scale * gesture.pan_y + focus_y)
                .min(center_y)
                .max(center_y - scale * size.y as f32);
            split.scale = scale;
            view.invalidate(env);
        }
    }
}
