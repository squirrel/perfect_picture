// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{bindings, gesture::Gesture, types::Point};

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeKeyDown(
    env: jni::JNIEnv,
    caller: bindings::Context,
    key_code: i32,
    scan_code: i32,
    view: bindings::View,
) -> bool {
    let mut state = crate::state();
    let state = state.get();
    match state.gesture {
        Some(Gesture::Button(gesture)) => {
            state.gesture = None;
            state.key_bindings.insert(scan_code, gesture.action);
            return true;
        }
        Some(Gesture::Pointer(point)) => match key_code {
            57 | 58 => {
                if let Some(picture) = &mut state.picture {
                    if let Some(split_id) = picture.split_at_position(point.y) {
                        let split = picture.split(split_id);
                        let position_x = (point.x - split.pan_x) / split.scale;
                        let position_y = (point.y - split.pan_y) / split.scale;
                        if position_x >= 0.
                            && position_y >= 0.
                            && position_x <= picture.size.x as f32
                            && position_y <= picture.size.y as f32
                        {
                            picture.mask.value.points.push(crate::types::MaskPoint {
                                position: Point {
                                    x: position_x,
                                    y: position_y,
                                },
                                radius: picture.brush_size,
                            });
                            picture.mask_intersection = None;
                            state.notify_views(env);
                        }
                    }
                }
                return true;
            }
            67 => {
                if let Some(picture) = &mut state.picture {
                    if let Some(split_id) = picture.split_at_position(point.y) {
                        let split = picture.split(split_id);
                        let density = view.get_display_metrics(env).scaled_density(env);
                        let position_x = (point.x - split.pan_x) / split.scale;
                        let position_y = (point.y - split.pan_y) / split.scale;
                        let mut best_distance = 8. * density * split.scale;
                        best_distance *= best_distance;
                        let mut best_index = None;
                        let mask = &mut picture.mask.value;
                        for (index, item) in mask.points.iter().enumerate() {
                            let distance_x = item.position.x - position_x;
                            let distance_y = item.position.y - position_y;
                            let distance =
                                distance_x * distance_x + distance_y * distance_y - item.radius;
                            if distance < best_distance {
                                best_distance = distance;
                                best_index = Some(index);
                            }
                        }
                        if let Some(index) = best_index {
                            mask.points.swap_remove(index);
                            picture.mask_intersection = None;
                            state.notify_views(env);
                        }
                    }
                }
                return true;
            }
            _ => {
                if let crate::toolbars::Button::Some(gesture) =
                    crate::toolbars::button_at_point(env, state, point.x, point.y, view)
                {
                    state.key_bindings.insert(scan_code, gesture.action);
                    return true;
                }
            }
        },
        _ => {}
    }
    if let Some(&action) = state.key_bindings.get(&scan_code) {
        crate::action::perform_action(action, caller, env, state);
        true
    } else {
        false
    }
}
