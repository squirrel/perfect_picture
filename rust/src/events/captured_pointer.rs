// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{bindings, types::SplitId};

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeCapturedPointer(
    env: jni::JNIEnv,
    _caller: bindings::Context,
    meta_state: i32,
    distance_x: f32,
    distance_y: f32,
    view: bindings::View,
) {
    let mut state = crate::state();
    let state = state.get();
    let picture = match &mut state.picture {
        None => return,
        Some(value) => value,
    };
    let pointer = match state.gesture {
        Some(crate::gesture::Gesture::Pointer(point)) => point,
        _ => return,
    };
    let split_id = match picture.split_at_position(pointer.y) {
        None => return,
        Some(value) => value,
    };
    match meta_state & 4097 {
        1 => {
            let density = view.get_display_metrics(env).scaled_density(env);
            let buttons_size_y = (48. * density).round() as i32;
            let buttons_position_y_min = view.get_height(env) - state.inset_bottom - buttons_size_y;
            let viewport_size_x = view.get_width(env);
            let viewport_size_y = buttons_position_y_min - state.inset_top;
            let center_x = viewport_size_x as f32 / 2.;
            let center_y = if picture.split_screen {
                match split_id {
                    SplitId::Split1 => state.inset_top + picture.split_position_y,
                    SplitId::Split2 => buttons_position_y_min + picture.split_position_y + 1,
                }
            } else {
                state.inset_top + viewport_size_y
            } as f32
                / 2.;
            let size = picture.size;
            let split = picture.split_mut(split_id);
            split.pan_x = (split.pan_x + distance_x)
                .min(center_x)
                .max(center_x - split.scale * size.x as f32);
            split.pan_y = (split.pan_y + distance_y)
                .min(center_y)
                .max(center_y - split.scale * size.y as f32);
            state.notify_views(env);
        }
        4096 => {
            if let Some(crate::gesture::Gesture::Pointer(point)) = state.gesture {
                let density = view.get_display_metrics(env).scaled_density(env);
                let buttons_size_y = (48. * density).round() as i32;
                let buttons_position_y_min =
                    view.get_height(env) - state.inset_bottom - buttons_size_y;
                let viewport_size_x = view.get_width(env);
                let viewport_size_y = buttons_position_y_min - state.inset_top;
                let default_scale =
                    if picture.size.x * viewport_size_y > picture.size.y * viewport_size_x {
                        viewport_size_x as f32 / picture.size.x as f32
                    } else {
                        viewport_size_y as f32 / picture.size.y as f32
                    };
                let center_x = viewport_size_x as f32 / 2.;
                let center_y = if picture.split_screen {
                    match split_id {
                        SplitId::Split1 => state.inset_top + picture.split_position_y,
                        SplitId::Split2 => buttons_position_y_min + picture.split_position_y + 1,
                    }
                } else {
                    state.inset_top + viewport_size_y
                } as f32
                    / 2.;
                let size = picture.size;
                let split = picture.split_mut(split_id);
                let pan_x = (split.pan_x - point.x) / split.scale;
                let pan_y = (split.pan_y - point.y) / split.scale;
                split.scale = (split.scale * 1.002f32.powf(-distance_y))
                    .min(64.)
                    .max(default_scale / 2.);
                split.pan_x = (split.scale * pan_x + point.x)
                    .min(center_x)
                    .max(center_x - split.scale * size.x as f32);
                split.pan_y = (split.scale * pan_y + point.y)
                    .min(center_y)
                    .max(center_y - split.scale * size.y as f32);
                state.notify_views(env);
            }
        }
        _ => {}
    }
}
