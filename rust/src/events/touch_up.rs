// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::gesture::Gesture;

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeTouchUp(
    env: jni::JNIEnv,
    _caller: crate::bindings::Context,
    shift: bool,
    position_x: f32,
    position_y: f32,
) {
    let mut state = crate::state();
    let state = state.get();
    if let Some(picture) = &mut state.picture {
        match &mut state.gesture {
            Some(Gesture::CropEdge(gesture)) => {
                picture.crop(*gesture, position_x, position_y);
                state.notify_views(env);
            }
            Some(Gesture::DrawMask { points, split_id }) => {
                let split = picture.split(*split_id);
                match &points[..] {
                    [point] => {
                        if point.x >= 0.
                            && point.y >= 0.
                            && point.x <= picture.size.x as f32
                            && point.y <= picture.size.y as f32
                        {
                            picture.mask.value.points.push(crate::types::MaskPoint {
                                position: point.into(),
                                radius: picture.brush_size,
                            });
                            picture.mask_intersection = None;
                            state.notify_views(env);
                        }
                    }
                    _ => {
                        let polygon = geo::Polygon::new(std::mem::take(points).into(), vec![]);
                        let polygon = geo::algorithm::simplify::Simplify::simplify(
                            &polygon,
                            &(0.5 / split.scale.min(1.)),
                        );
                        let mask = &mut picture.mask.value;
                        mask.polygon = if shift {
                            geo_booleanop::boolean::BooleanOp::difference(&mask.polygon, &polygon)
                        } else {
                            geo_booleanop::boolean::BooleanOp::union(&mask.polygon, &polygon)
                        };
                        picture.mask_intersection = None;
                        state.notify_views(env);
                    }
                }
                state.gesture = None;
            }
            _ => {}
        }
    }
}
