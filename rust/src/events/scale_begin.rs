// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::gesture;

#[allow(non_snake_case)]
#[no_mangle]
extern "C" fn Java_perfect_picture_Activity_nativeScaleBegin(
    _env: jni::JNIEnv,
    _caller: crate::bindings::Context,
    focus_x: f32,
    focus_y: f32,
    span: f32,
) {
    let mut state = crate::state();
    let state = state.get();
    if let Some(picture) = &mut state.picture {
        if let Some(split_id) = picture.split_at_position(focus_y) {
            let split = picture.split(split_id);
            state.gesture = Some(gesture::Gesture::Pan(gesture::Pan {
                pan_x: (split.pan_x - focus_x) / split.scale,
                pan_y: (split.pan_y - focus_y) / split.scale,
                scale: split.scale / span,
                split_id,
            }));
        }
    }
}
