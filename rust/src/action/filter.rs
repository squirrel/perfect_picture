// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{bindings, types::Choice};
use texture_synthesis::image;

pub enum Filter {
    BandStop { course: u16, fine: u16, opacity: u8 },
    Blur { radius: u16 },
    Levels { levels: crate::types::Levels },
    Light(crate::types::Light),
    UnsharpMask { radius: u16, threshold: u16 },
}

pub fn band_stop(env: jni::JNIEnv, picture: &mut crate::types::Picture) {
    apply(
        env,
        picture.band_stop_feather1,
        picture.band_stop_feather2,
        Filter::BandStop {
            course: picture.band_stop_course,
            fine: picture.band_stop_fine,
            opacity: picture.band_stop_opacity,
        },
        picture,
    );
}

pub fn blur(env: jni::JNIEnv, picture: &mut crate::types::Picture) {
    apply(
        env,
        picture.blur_feather1,
        picture.blur_feather2,
        Filter::Blur {
            radius: picture.blur_radius,
        },
        picture,
    );
}

pub fn levels_filter(env: jni::JNIEnv, picture: &mut crate::types::Picture) {
    apply(
        env,
        picture.levels_filter_feather1,
        picture.levels_filter_feather2,
        Filter::Levels {
            levels: picture.levels_filter_levels,
        },
        picture,
    );
}

pub fn light(env: jni::JNIEnv, picture: &mut crate::types::Picture) {
    if !picture.light.is_identity() {
        apply(
            env,
            picture.light_feather1,
            picture.light_feather2,
            Filter::Light(picture.light),
            picture,
        );
    }
}

pub fn unsharp_mask(env: jni::JNIEnv, picture: &mut crate::types::Picture) {
    apply(
        env,
        picture.unsharp_mask_feather1,
        picture.unsharp_mask_feather2,
        Filter::UnsharpMask {
            radius: picture.unsharp_mask_radius,
            threshold: picture.unsharp_mask_threshold,
        },
        picture,
    );
}

fn apply(
    env: jni::JNIEnv,
    feather1: u16,
    feather2: u16,
    filter: Filter,
    picture: &mut crate::types::Picture,
) {
    if picture.working {
        return;
    }
    if picture.mask.value.is_empty() {
        super::undo(env, picture);
        return;
    }
    let crop = if picture.intersect_masks {
        match picture.mask_intersection_bounding_box((2 * feather1) as _, (2 * feather2) as _) {
            None => return,
            Some(value) => value,
        }
    } else {
        picture.mask_bounding_box((2 * feather1) as _)
    };
    let image = picture.image.clone();
    let mask = picture.mask.value.clone();
    let mask_choice = picture.mask.choice;
    let intersection_mask = if picture.intersect_masks {
        Some(picture.mask.alternative_value.clone())
    } else {
        None
    };
    picture.working = true;
    crate::spawn(env, move |env| {
        let image = image.as_obj();
        let crop_size = crop.size();
        let cropped_image = bindings::Bitmap::create_bitmap_cropped(
            env,
            image,
            crop.position_x_min,
            crop.position_y_min,
            crop_size.x,
            crop_size.y,
        );
        let buffer_size = 4 * crop_size.x as usize * crop_size.y as usize;
        let mut undo_image_data = vec![0; buffer_size];
        let buffer = env.new_direct_byte_buffer(&mut undo_image_data).unwrap();
        cropped_image.copy_pixels_to_buffer(env, buffer.into());
        match filter {
            Filter::BandStop { .. } | Filter::Blur { .. } | Filter::UnsharpMask { .. } => {
                let source_image = image::RgbaImage::from_raw(
                    crop_size.x as _,
                    crop_size.y as _,
                    undo_image_data.clone(),
                )
                .unwrap();
                let filtered_image = match filter {
                    Filter::BandStop { course, fine, .. } => {
                        let medium = image::imageops::blur(&source_image, fine as _);
                        let course = image::imageops::blur(&medium, course as _).into_raw();
                        let medium = medium.into_raw();
                        let mut source = source_image.into_raw();
                        for ((source, medium), course) in source
                            .chunks_exact_mut(4)
                            .zip(medium.chunks_exact(4))
                            .zip(course.chunks_exact(4))
                        {
                            for index in 0..3 {
                                source[index] = (source[index] as i16)
                                    .wrapping_sub(medium[index] as i16)
                                    .wrapping_add(course[index] as i16)
                                    .min(255)
                                    .max(0) as u8;
                            }
                        }
                        image::RgbaImage::from_raw(crop_size.x as _, crop_size.y as _, source)
                            .unwrap()
                    }
                    Filter::Blur { radius } => image::imageops::blur(&source_image, radius as _),
                    Filter::UnsharpMask { radius, threshold } => {
                        image::imageops::unsharpen(&source_image, radius as _, threshold as _)
                    }
                    _ => panic!(),
                };
                let mut image_data = filtered_image.into_raw();
                let buffer = env.new_direct_byte_buffer(&mut image_data).unwrap();
                cropped_image.copy_pixels_from_buffer(env, buffer.into());
            }
            Filter::Levels { .. } | Filter::Light(_) => {}
        }
        let canvas = bindings::Canvas::new(env, image);
        let paint = bindings::Paint::new(env);
        match intersection_mask {
            None => {
                paint.set_color(
                    env,
                    match filter {
                        Filter::BandStop { opacity, .. } => (opacity as u32) << 24 | 0xffffff,
                        _ => 0xffffffff,
                    },
                );
                if feather1 > 0 {
                    let filter = bindings::BlurMaskFilter::new(
                        env,
                        feather1 as f32,
                        bindings::Blur::normal(env),
                    );
                    paint.set_mask_filter(env, filter.into());
                }
                let transfer_mode = bindings::PorterDuffTransferMode::new(
                    env,
                    bindings::PorterDuffMode::destination_out(env),
                );
                paint.set_transfer_mode(env, transfer_mode.into());
                canvas.draw_path(env, mask.path(env), paint);
                paint.set_color(env, 0xffffffff);
                paint.set_mask_filter(env, bindings::null());
            }
            Some(intersection_mask) => {
                let mask_image = bindings::Bitmap::create_bitmap_uninitialized(
                    env,
                    crop_size,
                    bindings::BitmapConfig::argb_8888(env),
                );
                let mask_canvas = bindings::Canvas::new(env, mask_image);
                let transfer_mode =
                    bindings::PorterDuffTransferMode::new(env, bindings::PorterDuffMode::add(env));
                paint.set_transfer_mode(env, transfer_mode.into());
                let (mask1, mask2) = match mask_choice {
                    Choice::Setting1 => (&mask, &intersection_mask),
                    Choice::Setting2 => (&intersection_mask, &mask),
                };
                if feather1 > 0 {
                    let filter = bindings::BlurMaskFilter::new(
                        env,
                        feather1 as f32,
                        bindings::Blur::normal(env),
                    );
                    paint.set_mask_filter(env, filter.into());
                }
                paint.set_color(env, 0xffff0000);
                let origin = crop.point_min_negative();
                let path = mask1.path_transform(env, origin, 1.);
                mask_canvas.draw_path(env, path, paint);
                let filter = if feather2 > 0 {
                    bindings::BlurMaskFilter::new(env, feather2 as f32, bindings::Blur::normal(env))
                        .into()
                } else {
                    bindings::null()
                };
                paint.set_mask_filter(env, filter);
                paint.set_color(env, 0xff00ff00);
                let path = mask2.path_transform(env, origin, 1.);
                mask_canvas.draw_path(env, path, paint);
                paint.set_color(env, 0xffff0000);
                paint.set_mask_filter(env, bindings::null());
                paint.set_transfer_mode(env, bindings::null());
                canvas.clip_rectangle(env, crop);
                let transfer_mode = bindings::PorterDuffTransferMode::new(
                    env,
                    bindings::PorterDuffMode::destination_out(env),
                );
                paint.set_transfer_mode(env, transfer_mode.into());
                let color_matrix = env.new_float_array(20).unwrap();
                #[rustfmt::skip]
                let matrix = [
                    0., 0., 0., 0., 0.,
                    0., 0., 0., 0., 0.,
                    0., 0., 0., 0., 0.,
                    1., 1., 0., 0., -255.,
                ];
                env.set_float_array_region(color_matrix, 0, &matrix)
                    .unwrap();
                paint.set_color_filter(
                    env,
                    bindings::ColorMatrixColorFilter::new(env, color_matrix.into()).into(),
                );
                canvas.draw_bitmap_simple(env, mask_image, crop.point_min(), paint);
                paint.set_color_filter(env, bindings::null());
            }
        }
        match filter {
            Filter::BandStop { .. } | Filter::Blur { .. } | Filter::UnsharpMask { .. } => {}
            Filter::Levels { levels } => {
                paint.set_color_filter(env, levels.color_filter(env));
            }
            Filter::Light(parameters) => {
                let color_filter =
                    crate::bindings::ColorMatrixColorFilter::new(env, parameters.matrix(env))
                        .into();
                paint.set_color_filter(env, color_filter);
            }
        }
        let transfer_mode = bindings::PorterDuffTransferMode::new(
            env,
            bindings::PorterDuffMode::destination_over(env),
        );
        paint.set_transfer_mode(env, transfer_mode.into());
        canvas.draw_bitmap_simple(env, cropped_image, crop.point_min(), paint);
        let mut state = crate::state();
        let state = state.get();
        let picture = match &mut state.picture {
            None => return,
            Some(value) => value,
        };
        picture.clear_mask();
        picture.undo = Some(crate::types::Undo {
            bounding_box: crop,
            image_data: undo_image_data.into(),
            mask,
        });
        picture.working = false;
        state.notify_views(env);
    });
}
