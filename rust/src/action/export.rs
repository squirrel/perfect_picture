// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::{bindings, types::Format};

pub fn export(context: bindings::Context, env: jni::JNIEnv, share: bool) {
    let context = context.global(env);
    crate::spawn(env, move |env| {
        let mut state_lock = crate::state();
        let state = state_lock.get();
        let picture = match &state.picture {
            None => return,
            Some(value) => value,
        };
        let (format, mime_type) = match picture.export_format {
            Format::Jpeg => (bindings::CompressFormat::jpeg(env), "image/jpeg"),
            Format::Png => (bindings::CompressFormat::png(env), "image/png"),
            Format::Webp => (bindings::CompressFormat::webp(env), "image/webp"),
        };
        let image = picture.image.clone();
        let quality = picture.export_quality;
        let scaled_down_size = if picture.export_scale_down {
            picture.scaled_down_size()
        } else {
            None
        };
        let crop = picture.crop.value;
        let color_filter = picture.levels.value.color_filter(env);
        drop(state_lock);
        let paint = bindings::Paint::new(env);
        paint.set_color_filter(env, color_filter);
        let crop_size = crop.size();
        let output_size = scaled_down_size.unwrap_or(crop_size);
        let output = bindings::Bitmap::create_bitmap_uninitialized(
            env,
            output_size,
            bindings::BitmapConfig::argb_8888(env),
        );
        let canvas = bindings::Canvas::new(env, output);
        let source = bindings::Rect::new(
            env,
            crop.position_x_min,
            crop.position_y_min,
            crop.position_x_max,
            crop.position_y_max,
        );
        let destination = bindings::RectF::new(env, 0., 0., output_size.x as _, output_size.y as _);
        canvas.draw_bitmap(env, image.as_obj(), source, destination, paint);
        context.as_obj().export(
            env,
            format,
            output,
            bindings::string(env, mime_type),
            quality as _,
            share,
        );
    });
}
