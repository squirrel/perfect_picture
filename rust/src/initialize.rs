// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use jni::{objects::JClass, JNIEnv};
use std::fmt::Write;

pub struct Lock {
    inner: std::sync::MutexGuard<'static, Option<State>>,
}

pub struct State {
    class: jni::objects::GlobalRef,
    pub gesture: Option<crate::gesture::Gesture>,
    pub inset_bottom: i32,
    pub inset_top: i32,
    pub key_bindings: std::collections::HashMap<i32, crate::action::Action>,
    pub picture: Option<crate::types::Picture>,
}

lazy_static::lazy_static! {
    static ref STATE: std::sync::Mutex<Option<State>> = Default::default();
}

pub fn state() -> Lock {
    Lock {
        inner: STATE.lock().unwrap(),
    }
}

impl Lock {
    pub fn get(&mut self) -> &mut State {
        self.inner.as_mut().unwrap()
    }
}

impl State {
    pub fn notify_views(&self, env: JNIEnv) {
        env.call_static_method(JClass::from(self.class.as_obj()), "notifyViews", "()V", &[])
            .unwrap();
    }
}

#[allow(non_snake_case)]
#[no_mangle]
fn Java_perfect_picture_Activity_nativeInitialize(env: JNIEnv, class: JClass) {
    let jvm = env.get_java_vm().unwrap();
    std::panic::set_hook(Box::new(move |panic_info| {
        let env = jvm.attach_current_thread().unwrap();
        let mut message = panic_info.to_string();
        if env.exception_check().unwrap() {
            let exception = env.exception_occurred().unwrap();
            env.exception_clear().unwrap();
            let exception = env
                .call_method(exception, "toString", "()Ljava/lang/String;", &[])
                .unwrap()
                .l()
                .unwrap();
            write!(
                &mut message,
                "\nexception: {}",
                env.get_string(exception.into()).unwrap().to_str().unwrap(),
            )
            .unwrap();
        }
        env.fatal_error(message);
    }));
    *state().inner = Some(State {
        class: env.new_global_ref(class).unwrap(),
        gesture: None,
        inset_bottom: 0,
        inset_top: 0,
        key_bindings: Default::default(),
        picture: None,
    });
}
