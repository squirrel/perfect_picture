// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

#[derive(Clone, Copy)]
pub struct Light {
    pub brightness: i8,
    pub saturation: i8,
}

impl Light {
    pub fn is_identity(self) -> bool {
        self.brightness == 0 && self.saturation == 0
    }

    pub fn matrix(self, env: jni::JNIEnv) -> jni::objects::JObject {
        let brightness = self.brightness as f32 / 100. + 1.;
        let saturation = self.saturation as f32 / 100. + 1.;
        let inverted_saturation = brightness * (1. - saturation);
        let red = 0.213 * inverted_saturation;
        let green = 0.715 * inverted_saturation;
        let blue = 0.072 * inverted_saturation;
        #[rustfmt::skip]
        let matrix = [
            brightness * (red + saturation), green, blue, 0., 0.,
            red, brightness * (green + saturation), blue, 0., 0.,
            red, green, brightness * (blue + saturation), 0., 0.,
            0., 0., 0., 1., 0.,
        ];
        let matrix_array = env.new_float_array(20).unwrap();
        env.set_float_array_region(matrix_array, 0, &matrix)
            .unwrap();
        matrix_array.into()
    }
}
