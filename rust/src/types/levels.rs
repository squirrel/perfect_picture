// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::gesture::{self, Channel};

#[derive(Clone, Copy)]
pub struct Levels {
    pub blue: (u8, u8),
    pub green: (u8, u8),
    pub red: (u8, u8),
}

impl Levels {
    pub fn apply(&mut self, gesture: crate::gesture::Levels, level: f32) {
        let level = level.round().max(0.).min(255.) as u8;
        let apply = |levels: &mut (u8, u8)| match gesture.extreme {
            gesture::Extreme::Max => {
                levels.1 = level.max(levels.0 + 1);
            }
            gesture::Extreme::Min => {
                levels.0 = level.min(levels.1 - 1);
            }
        };
        match gesture.channel {
            Channel::Master => {
                apply(&mut self.red);
                apply(&mut self.green);
                apply(&mut self.blue);
            }
            Channel::Red => {
                apply(&mut self.red);
            }
            Channel::Green => {
                apply(&mut self.green);
            }
            Channel::Blue => {
                apply(&mut self.blue);
            }
        }
    }

    pub fn color_filter<'a>(&self, env: jni::JNIEnv<'a>) -> crate::bindings::ColorFilter<'a> {
        crate::bindings::ColorMatrixColorFilter::new(env, self.color_matrix_array(env)).into()
    }

    pub fn color_matrix<'a>(&self, env: jni::JNIEnv<'a>) -> crate::bindings::ColorMatrix<'a> {
        crate::bindings::ColorMatrix::new(env, self.color_matrix_array(env))
    }

    fn color_matrix_array<'a>(&self, env: jni::JNIEnv<'a>) -> jni::objects::JObject<'a> {
        let levels = |levels: (u8, u8)| {
            let contrast = 255. / (levels.1 - levels.0) as f32;
            (contrast, -contrast * levels.0 as f32)
        };
        let levels_red = levels(self.red);
        let levels_green = levels(self.green);
        let levels_blue = levels(self.blue);
        let color_matrix = env.new_float_array(20).unwrap();
        #[rustfmt::skip]
        let matrix = [
            levels_red.0, 0., 0., 0., levels_red.1,
            0., levels_green.0, 0., 0., levels_green.1,
            0., 0., levels_blue.0, 0., levels_blue.1,
            0., 0., 0., 1., 0.,
        ];
        env.set_float_array_region(color_matrix, 0, &matrix)
            .unwrap();
        color_matrix.into()
    }

    pub fn master(&self) -> (u8, u8) {
        (
            ((self.red.0 as f32 + self.green.0 as f32 + self.blue.0 as f32) / 3.).round() as _,
            ((self.red.1 as f32 + self.green.1 as f32 + self.blue.1 as f32) / 3.).round() as _,
        )
    }
}

impl Default for Levels {
    fn default() -> Self {
        Levels {
            blue: (0, 255),
            green: (0, 255),
            red: (0, 255),
        }
    }
}
