// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

#[derive(Clone, Copy, PartialEq)]
pub enum Choice {
    Setting1,
    Setting2,
}

pub struct Setting<T> {
    pub alternative_value: T,
    pub choice: Choice,
    pub value: T,
}

impl<T: Clone> Setting<T> {
    pub fn new(value: T) -> Self {
        Setting {
            alternative_value: value.clone(),
            choice: Choice::Setting1,
            value: value,
        }
    }

    pub fn set_choice(&mut self, choice: Choice) {
        if choice != self.choice {
            self.choice = choice;
            std::mem::swap(&mut self.alternative_value, &mut self.value);
        }
    }
}
