// Perfect Picture
// Copyright: Contributors
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

use crate::types::{Point, SplitId};
use std::cmp::Ordering;

#[derive(Clone, Copy)]
pub enum Channel {
    Blue,
    Green,
    Master,
    Red,
}

#[derive(Clone, Copy)]
pub enum Extreme {
    Max,
    Min,
}

pub enum Gesture {
    Button(crate::types::ButtonGesture),
    CloneOffset {
        position: Point,
        split_id: SplitId,
    },
    CropCenter {
        position: Point,
        split_id: SplitId,
    },
    CropEdge(CropEdge),
    DrawMask {
        points: Vec<geo::Coordinate<f32>>,
        split_id: SplitId,
    },
    Levels(Levels),
    LevelsFilter(Levels),
    Pan(Pan),
    Pointer(Point),
    Split,
}

#[derive(Clone, Copy)]
pub struct CropEdge {
    pub attachment_x: Ordering,
    pub attachment_y: Ordering,
    pub split_id: SplitId,
}

#[derive(Clone, Copy)]
pub struct Levels {
    pub channel: Channel,
    pub extreme: Extreme,
}

pub struct Pan {
    pub pan_x: f32,
    pub pan_y: f32,
    pub scale: f32,
    pub split_id: SplitId,
}
